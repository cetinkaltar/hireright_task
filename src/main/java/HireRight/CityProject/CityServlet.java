package HireRight.CityProject;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import HireRight.CityProject.dto.City;
import HireRight.CityProject.dto.Weather;

@WebServlet("/api")
public class CityServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final static Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public CityServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String cityName = request.getParameter("name");

		if (cityName != null) {
			response.setContentType("application/json; charset=UTF-8");
			response.getWriter().append(cityName).append("  Weather Info: ").append(getWeather(cityName).toString());
		} else {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String zip = request.getParameter("zip");

		if (zip != null) {
			response.setContentType("application/json; charset=UTF-8");
			response.getWriter().append(zip).append("  City Time Zone: ").append(getTimeZone(zip).toString());
		} else {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
	}

	private Weather getWeather(String cityName) {
		List<City> cities = getCities();
		for (City city : cities) {
			if (city.getName().equals(cityName)) {
				return (Weather) city.getWeather();
			}
		}

		return new Weather();
	}

	private String getTimeZone(String zip) {
		List<City> cities = getCities();
		for (City city : cities) {
			if (city.getZip().equals(zip)) {
				return city.getTimeZone();
			}
		}

		return "No TimeZone Data!";
	}

	private List<City> getCities() {
		JsonReader reader;
		List<City> cities = new ArrayList<City>();
		try {
			reader = new JsonReader(new FileReader("data.json"));
			City[] cityList = new Gson().fromJson(reader, City[].class);
			cities = Arrays.asList(cityList);
		} catch (FileNotFoundException e) {
			log.info("File not found!");
			e.printStackTrace();
		}

		return cities;
	}
}
