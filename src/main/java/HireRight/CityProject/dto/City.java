package HireRight.CityProject.dto;

public class City {
	private String name;
	private String zip;
	private String timeZone;
	private Weather weather;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public Weather getWeather() {
		return weather;
	}

	public void setWeather(Weather weather) {
		this.weather = weather;
	}

	@Override
	public String toString() {
		return "City{" + "name='" + name + '\'' + ", zip=" + zip + ", timeZone='" + timeZone + '\'' + ", weather="
				+ weather + '}';
	}
}
